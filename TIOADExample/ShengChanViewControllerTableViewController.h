//
//  ShengChanViewControllerTableViewController.h
//  TIOADExample
//
//  Created by Li Xianyu on 14/11/25.
//  Copyright (c) 2014年 Texas Instruments. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import "PhobosDetailViewController.h"

@protocol ShengChanDelegate <NSObject>

-(void) didSelectDevice:(CBCharacteristic*)imageNotiy imageBlock:(CBCharacteristic*)imageBlock;

@end
@interface ShengChanViewControllerTableViewController : UITableViewController<CBCentralManagerDelegate, CBPeripheralDelegate>
@property (strong,nonatomic) NSMutableArray *devices;
@property (strong,nonatomic) CBCentralManager *manager;
@property (strong,nonatomic) CBPeripheral *p;
@property (strong,nonatomic) CBCharacteristic *cFirmwareRevision;
@property (strong,nonatomic) CBCharacteristic *cSerialNumber;
@property (strong,nonatomic) CBCharacteristic *cReset;
@property (strong,nonatomic) PhobosDetailViewController *pvc;
@property (strong,nonatomic) NSString *batteryLeavel;
@property id<ShengChanDelegate> delegate;
@end
