//
//  ShengChanViewControllerTableViewController.m
//  TIOADExample
//
//  Created by Li Xianyu on 14/11/25.
//  Copyright (c) 2014年 Texas Instruments. All rights reserved.
//

#import "ShengChanViewControllerTableViewController.h"
#import "PhobosDetailViewController.h"

@interface ShengChanViewControllerTableViewController ()
@property (strong,nonatomic) UIWindow *bWindow;
@end

@implementation ShengChanViewControllerTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    NSLog(@"%s", __func__);
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
        //self.devices = [[NSMutableArray alloc]init];
    }
    return self;
}

- (void)viewDidLoad
{
    NSLog(@"%s", __func__);
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    NSLog(@"%s", __func__);
    for (CBPeripheral *peripheral in _devices) {
        if (!(peripheral.state == CBPeripheralStateDisconnected)) {
            [_manager cancelPeripheralConnection:peripheral];
        }
    }
    _devices = [NSMutableArray arrayWithCapacity:1];
//    [self performSelector:@selector(installBrightnessWindow) withObject:nil afterDelay:3.0];
}

-(void)viewDidAppear:(BOOL)animated {
    NSLog(@"%s", __func__);
    self.manager.delegate = self;
    NSDictionary *optionsdict = @{CBCentralManagerScanOptionAllowDuplicatesKey : [NSNumber numberWithBool:YES]};
    //    [_bleManager scanForPeripheralsWithServices:nil options:optionsdict];
    [self.manager scanForPeripheralsWithServices:nil options:optionsdict];
}

- (void)didReceiveMemoryWarning
{
    NSLog(@"%s", __func__);
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)installBrightnessWindow {
    NSLog(@"%s", __func__);
    
    [UIScreen mainScreen].brightness = 0.8;
}

- (void)reConnect:(CBPeripheral*)peripheral {
    NSLog(@"%s", __func__);
    [self.manager connectPeripheral:peripheral options:nil];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
//    NSLog(@"%s", __func__);
    // Return the number of sections.
    return 2;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
//    NSLog(@"%s", __func__);
    if (section == 0) return @"Actions";
    else if (section == 1) return @"Devices Found";
    else return @"";
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//    NSLog(@"%s", __func__);
    // Return the number of rows in the section.
    if (section == 0) return 1;
    else if (section == 1) return self.devices.count;
    else return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    NSLog(@"%s", __func__);
    UITableViewCell *cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
    
    // Configure the cell...
    if (indexPath.section == 0) {
        cell.textLabel.text = @"Cancel";
    }
    if (indexPath.section == 1) {
        CBPeripheral *p = [self.devices objectAtIndex:indexPath.row];
        cell.textLabel.text = [NSString stringWithFormat:@"%@ - %@", p.name, _batteryLeavel];
    }
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"%s", __func__);
    if (indexPath.section == 1)
    {
        self.p = [self.devices objectAtIndex:indexPath.row];
//        [self.delegate didSelectDevice:_cFirmwareRevision imageBlock:_cSerialNumber];
        _pvc = [[PhobosDetailViewController alloc] init];
        [self presentViewController:_pvc animated:YES completion:nil];
    }
    else {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

#pragma mark - CBCentralManagerDelegate Callbacks

-(void) centralManagerDidUpdateState:(CBCentralManager *)central {
    NSLog(@"%s", __func__);
}

-(void) centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI {
    NSLog(@"%s, RSSI = %@, name = %@, uuid = %@", __func__, RSSI, peripheral.name, peripheral.identifier.UUIDString);
    int iRssi = [RSSI intValue];
    NSLog(@"iRssi = %d", iRssi);
    // 只有当手机离Phobos很近的时候，才会识别。
    // 这样做是为了避免周围有很多Phobos，对配对产生混乱。
    if (iRssi < -59 || iRssi > 0) {
        return;
    }
    if ([self.devices containsObject:peripheral]) {
        if (peripheral.state == CBPeripheralStateDisconnected) {
            NSLog(@"OH, Let's reconnect it!");
            [self performSelector:@selector(reConnect:) withObject:peripheral afterDelay:4.8];
            return;
        }
    }
    else {
        if (self.devices.count < 1) { // 因为我周围有20多个iBeacon，我不想影响她们
            [self.devices addObject:peripheral];
            [self.manager connectPeripheral:peripheral options:nil];
            //[self.manager stopScan];
        }
    }
}

-(void) centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral {
    NSLog(@"%s", __func__);
    peripheral.delegate = self;
#if 1
    [peripheral discoverServices:nil];
#else
    [self.manager cancelPeripheralConnection:peripheral];
    [self.tableView reloadData];
#endif
    [central stopScan];
}

- (void)letUsreloadData {
    NSLog(@"%s", __func__);
    //    [self.tableView performSelector:@selector(reloadData) withObject:nil afterDelay:1.0];
    [self.tableView reloadData];
}

#pragma mark - CBPeripheralDelegate Callbacks

-(void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error {
    NSLog(@"%s", __func__);
    //    [self.manager cancelPeripheralConnection:peripheral];
    //    [self.tableView reloadData];
    for (CBService *s in peripheral.services) {
        if ([s.UUID isEqual:[CBUUID UUIDWithString:@"180F"]]) { // Battery service
            [peripheral discoverCharacteristics:nil forService:s];
        }
        else if ([s.UUID isEqual:[CBUUID UUIDWithString:@"180A"]]) {//Device Information
            [peripheral discoverCharacteristics:nil forService:s];
        }
        else if ([s.UUID isEqual:[CBUUID UUIDWithString:@"1802"]]) {//Immediate alert
            [peripheral discoverCharacteristics:nil forService:s];
        }
        else if ([s.UUID isEqual:[CBUUID UUIDWithString:@"013d8e3b-1877-4d5c-bc59-aaa7e5082346"]]) {
            [peripheral discoverCharacteristics:nil forService:s];
            [self performSelector:@selector(letUsreloadData) withObject:nil afterDelay:2.0];
        }
    }
}

-(void) peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error {
    NSLog(@"%s", __func__);
    NSLog(@"Service : %@", service.UUID);
    NSLog(@"Characteristic : %@", service.characteristics);
    if ([service.UUID isEqual:[CBUUID UUIDWithString:@"180A"]]) {//Device Information
        for (CBCharacteristic *characteristic in service.characteristics) {
            if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:@"2A26"]]) {//Firmware Revision String
                _cFirmwareRevision = characteristic;
                [peripheral readValueForCharacteristic:characteristic];
            }
            else if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:@"2A25"]]) {//Serial Number String
                _cSerialNumber = characteristic;
                [peripheral readValueForCharacteristic:characteristic];
            }
        }
    }
    else if ([service.UUID isEqual:[CBUUID UUIDWithString:@"180F"]]) {// Battery service
        for (CBCharacteristic *characteristic in service.characteristics) {
            if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:@"2A19"]]) {
                [peripheral readValueForCharacteristic:characteristic];
            }
        }
    }
    else if ([service.UUID isEqual:[CBUUID UUIDWithString:@"1802"]]) {//Immediate alert
        for (CBCharacteristic *characteristic in service.characteristics) {
            if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:@"2A06"]]) {
                Byte byteValue = 0x02;
                NSData *d = [[NSData alloc] initWithBytes:&byteValue length:1];
                [peripheral writeValue:d forCharacteristic:characteristic type:CBCharacteristicWriteWithoutResponse];
            }
        }
    }
    else if ([service.UUID isEqual:[CBUUID UUIDWithString:@"013d8e3b-1877-4d5c-bc59-aaa7e5082346"]]) {
        for (CBCharacteristic *characteristic in service.characteristics) {
            if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:@"6ee16f2c-fcb7-4ca0-b416-7718a6f0687c"]]) {
                _cReset = characteristic;
            }
        }
    }
}

- (void) resetThePhobos:(CBPeripheral*)peripheral {
    NSLog(@"%s", __func__);
    uint8_t connData[7];
    
    // password is : 0x60,0x78,0xB6,0xA0,0x94,0x15
    connData[0] = 0x60; connData[1] = 0x78;
    connData[2] = 0xB6; connData[3] = 0xA0;
    connData[4] = 0x94; connData[5] = 0x15;
    connData[6] = 0x00;//
    [peripheral writeValue:[NSData dataWithBytes:connData length:7] forCharacteristic:_cReset type:CBCharacteristicWriteWithoutResponse];
}

-(void) peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
    NSLog(@"%s", __func__);
    if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:@"2A25"]] ||
        [characteristic.UUID isEqual:[CBUUID UUIDWithString:@"2A26"]]) {
        unsigned char data[characteristic.value.length+2];
        [characteristic.value getBytes:&data];
        int i;
        for (i = 0; i < characteristic.value.length; i++) {
            NSLog(@"0x%02X", data[i]);
        }
        data[i] = 0;
        NSString *serialNumber = [NSString stringWithUTF8String:data];
        NSLog(@"serialNumber = %@", serialNumber);
    }
    else if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:@"2A19"]]) {
        char batlevel;
        [characteristic.value getBytes:&batlevel length:1];
        float theBattery = (float)batlevel;
        _batteryLeavel = [NSString stringWithFormat:@"Battery level:%0.1f%%", theBattery];
        NSLog(@"_batteryLevel = %f", theBattery);
        [self performSelector:@selector(letUsreloadData) withObject:nil afterDelay:1.6];
        NSLog(@"oh, we will reset the phobos after 20 seconds.");
        [self performSelector:@selector(resetThePhobos:) withObject:peripheral afterDelay:20.0];
    }
}
@end
